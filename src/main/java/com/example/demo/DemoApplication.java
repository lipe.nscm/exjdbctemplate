package com.example.demo;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.repository.ChaveRepository;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner{

    private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);

	@Autowired
    ChaveRepository chaveRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
			log.info("INICIADO A APLICAÇÃO DE EXEMPLO");
			runApp();
	}
	
	private void runApp() {
	
		List<Chave> chaves = Arrays.asList(
				new Chave(1L,"Saudade"),
				new Chave(2L,"Enferrujado"),
				new Chave(3L,"Dezessete"),
				new Chave(4L,"Aurora"),
				new Chave(5L,"Forno"),
				new Chave(6L,"Nove"),
				new Chave(7L,"Benigno")
				);
		
		log.info("[PERSISTINDO]");
		chaves.forEach(chave ->{
			log.info("Persistindo a chave {}", chave.getNomeChave());
			chaveRepository.save(chave);
		});
		
		log.info("[CONTAGEM] Total de chaves persistidas : {}", chaveRepository.count());
		
		log.info("[LISTAR TODOS] {}",chaveRepository.findAll());
		
		log.info("[PERSISTINDO VIA PROCEDURE]");
		chaveRepository.insertViaProc("JOSIE");

	}

}
