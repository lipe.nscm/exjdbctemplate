package com.example.demo.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.example.demo.Chave;

@Repository
public class JdbcChaveRepository implements ChaveRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int insertViaProc(String nomeChave) {
		SimpleJdbcCall sj = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_INSERE_NOVA_CHAVE");
		Map<String, Object> inParamMap = new HashMap<String, Object>();
		inParamMap.put("sp_nomechave", nomeChave);
		SqlParameterSource in = new MapSqlParameterSource(inParamMap);
		sj.execute(in);
		return 1;
	}
	
	@Override
	public int count() {
		return jdbcTemplate.queryForObject("select count(*) from chave", Integer.class);
	}

	@Override
	public int save(Chave chave) {
		return jdbcTemplate.update("insert into chave(nomeChave) values(?)", chave.getNomeChave());
	}

	@Override
	public int update(Chave chave) {
		return jdbcTemplate.update("update chave set nomeChave = ? where codigoChave = ?", chave.getNomeChave(), chave.getCodigoChave());
	}

	@Override
	public int deleteById(Long codigoChave) {
		return jdbcTemplate.update("delete chave where id = ?", codigoChave);
	}

	@Override
	public List<Chave> findAll() {

		return jdbcTemplate.query(
				
				"select * from chave"
				, (rs, rowNum) -> 
									new Chave(rs.getLong("codigoChave"),
											rs.getString("nomeChave")));
	}

	@Override
	public Optional<Chave> findById(Long codigoChave) {
		return jdbcTemplate.queryForObject(
				"select * from chave where codigoChave = ? ",
				new Object[] {codigoChave}
				, (rs, rowNum) -> Optional.of(new Chave(
						rs.getLong("codigoChave"),
						rs.getString("nomeChave")
						)));
	}

	@Override
	public String getChaveNameById(Long codigoChave) {
		return jdbcTemplate.queryForObject("select nomeChave from chave where codigoChave = ? ", 
				new Object[] {codigoChave}, String.class);
	}

}
