package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.example.demo.Chave;

@Repository
public interface ChaveRepository {

	
	int count();
	int save(Chave chave);
	int update(Chave chave);
	int deleteById(Long codigoChave);
	List<Chave> findAll();
	Optional<Chave>findById(Long codigoChave);
	String getChaveNameById(Long codigoChave);
	int insertViaProc(String nomeChave);
	
	
	}
